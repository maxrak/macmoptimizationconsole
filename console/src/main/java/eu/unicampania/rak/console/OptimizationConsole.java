package eu.unicampania.rak.console;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;
import java.util.function.BiConsumer;

import org.beryx.textio.AbstractTextTerminal;
import org.beryx.textio.EnumInputReader;
import org.beryx.textio.InputReader.ValueChecker;
import org.beryx.textio.system.SystemTextTerminal;
import org.beryx.textio.web.RunnerData;
import org.beryx.textio.TextIO;
import org.beryx.textio.TextIoFactory;
import org.beryx.textio.TextTerminal;

import eu.musaproject.multicloud.engines.deployments.MissingComponentsException;
import eu.musaproject.multicloud.engines.deployments.MissingOfferingException;
import eu.unicampania.rak.console.actions.Campaign;
import eu.unicampania.rak.console.actions.Experiment;
import eu.unicampania.rak.console.data.Crossovers;
import eu.unicampania.rak.console.data.Mutators;
import eu.unicampania.rak.console.data.Selectors;
import eu.unicampania.rak.console.data.actions;
import eu.unicampania.rak.console.data.algorithms;
import eu.unicampania.rak.console.data.parameters;
import eu.unicampania.rak.console.exceptions.noAvailableFilesException;

public class OptimizationConsole {
	//Optimizer Types
	static String[] deployers= {"GeneratorOptimizer2","GeneticOptimizer2"};
	
	//Genetic Alternatives
	static String[] selectors={"BoltzmannSelector", "EliteSelector", "ExponentialRankSelector", "LinearRankSelector", "MonteCarloSelector", "ProbabilitySelector", "RouletteWheelSelector", "StochasticUniversalSelector", "TournamentSelector", "TruncationSelector"};
	static String[] mutators={"Mutator", "GaussianMutator", "SwapMutator"};
	static String[] crossovers={"IntermediateCrossover", "LineCrossover", "MultiPointCrossover", "PartiallyMatchedCrossover", "UniformCrossover"};		
	static String[] params={"0.1","0.2","0.3","0.4","0.5","0.6","0.7","0.8","0.9"};		

	//problems
	static String[] macms={"1","311","535"};
	static String[] offerings={"5","10","15"};

	String MACMpath="macms/";
	String OfferingPath="offerings/";
	
	Experiment exp;
	PrintWriter output;
	Properties configuration;
	StringWriter stringOut;
	enum outputOptions {file, terminal};
	outputOptions selectedOutput=outputOptions.terminal;
	
	//Terminal Control
	TextTerminal terminal;
	
	public OptimizationConsole() throws MissingOfferingException, MissingComponentsException {
		exp=new Experiment();
		stringOut= new StringWriter();
		output = new PrintWriter(stringOut);
		configuration=defaultConf();
		terminal = new SystemTextTerminal();
	}
	
	protected void run(Properties p) throws IOException, MissingOfferingException, MissingComponentsException {
		exp.configure(p);
		String optData=exp.run();
		String problem=exp.problemDescriptor();
		String solution=exp.getSolution();
		showConfiguration();
		output.println("******************** MACM Optimizer Execution ************************");		
		output.println(problem);
		output.println(solution);
		output.println(optData);
	}
	
	protected void run() throws IOException, MissingOfferingException, MissingComponentsException {
		run(configuration);
	}
		
	protected PrintWriter setOutput(String out) throws FileNotFoundException {
		if (out.equalsIgnoreCase("stdout")) 
			output = new PrintWriter(output);
		else {
			File file = new File(out);
			output = new PrintWriter(file);
		}			
		return output;
	}
	
	
	protected String showOptions(){
		int i;
		output.println("**** MACM Optimizer ****");
		output.println("*** Options:");
		output.println("** Algorithms: " );
		for (i=0;i<deployers.length;i++) {
			output.print(" ["+i+"] "+deployers[i]);
		}
		output.println("");

		output.println("*MACMs: ");
		for (i=0;i<macms.length;i++) {
			output.print(" ["+i+"] "+macms[i]);
		}
		output.println("");

		output.println("*Offerings: " );
		for (i=0;i<offerings.length;i++) {
			output.print( "["+i+"] "+offerings[i]);
		}
		output.println("");
		output.println("** Genetic Algorithms Parameters " );
		output.println("*Selector " );
		for (i=0;i<selectors.length;i++) {
			output.print(" ["+i+"] "+selectors[i]);
		}
		output.println("");
		output.println("*Crossover " );
		for (i=0;i<crossovers.length;i++) {
			output.print(" ["+i+"] "+crossovers[i]);
		}
		output.println("");
		output.println("*Crossover Param " );
		for (i=0;i<params.length;i++) {
			output.print(" "+params[i]);
		}
		output.println("");
		output.println("*Mutator " );
		for (i=0;i<mutators.length;i++) {
			output.print(" ["+i+"] "+mutators[i]);
		}
		output.println("");
		output.println("*Mutator Param " );
		for (i=0;i<params.length;i++) {
			output.print(" "+params[i]);
		}
		output.println("");
		output.println("**** MACM Optimizer ****");
		return stringOut.toString();
	}
	
	protected void showConfiguration(Properties p){
		int i;
		output.println("******************** MACM Optimizer Configuration ************************");		
		output.println("* Algorithm selected: "+p.getProperty("DeployerType"));
		output.println("* MACM selected: "+p.getProperty("MACMfile"));
		output.println("* Offering selected: "+p.getProperty("Offeringfile") );
		if (configuration.getProperty("DeployerType").equals(""+algorithms.GeneticOptimizer2)) {
			output.println("**************** MACM Optimizer Genetic Configuration *******************");		
			
			output.println("* Population Size " +p.getProperty("PopulationSize"));
			output.println("* Selector " +p.getProperty("Selector"));
			output.println("* Crossover "+p.getProperty("Crossover") );
			output.println("* Crossover Param " +p.getProperty("CrossoverParam"));
			output.println("* Mutator "+p.getProperty("Mutator") );
			output.println("* Mutator Param "+p.getProperty("MutatorParam") );
			output.println("**** MACM Optimizer ****");			
		}
	}
	
	protected void showConfiguration() {
		showConfiguration(configuration);
	}
	
	public Properties defaultConf() {
		Properties p=new Properties();
		p.put("MACMfile","macms/1.macm");
		p.put("Offeringfile", "offerings/6.offering");
		p.setProperty("DeployerType",""+algorithms.GeneticOptimizer2);		
		p.setProperty("PopulationSize", "500");
		p.setProperty("Selector", "TruncationSelector");
		p.setProperty("Crossover", "SinglePointCrossover");
		p.setProperty("Mutator", "Mutator");
		p.setProperty("CrossoverParam", "0.1");
		p.setProperty("MutatorParam", "0.3");
		return p;
	}
	
	public void setConfiguration(Properties p) {
		configuration=p;
	}
	
	public void setOutputonFile(){		
		selectedOutput=outputOptions.file;
	}
	
	 protected int request() {
		int i=0;
		String action;
		Scanner scan=new Scanner(System.in);
		output.println("*********************");
		output.println("** Actions:");
		output.println("* 1. Run ");
		output.println("* 2. Change Deployer ");
		output.println("* 3. Change Genetic Conf ");
		output.println("* Select your choice: ");
		i=scan.nextInt();		
		return i;
	}
	 
	    private void actionSelector() throws MissingComponentsException, MissingOfferingException, IOException, noAvailableFilesException {
	        boolean go=true;
	    	TextIO textIO = new TextIO(terminal);
	        
	      
	        while(go) {	        
		        actions a = textIO.newEnumInputReader(actions.class)
		                .read("Which actions do you want to perform?");
		        
		        switch (a) {
		        	case showConfiguration: 
		        		outputOptions tmpselectedOutput=selectedOutput;
		        		selectedOutput=outputOptions.terminal;		        		
		        		showConfiguration(); 
		        		show(); 
		        		selectedOutput=tmpselectedOutput;
		        		break;
		        	case run: run(); show(); break;
		        	case selectMACM: selectMACM();break;
		        	case selectOffering: selecOffering();break;
		        	case changeAlgorithm: changeAlgorithm();break;
		        	case changeSelector: changeSelector();break;
		        	case changeMutator: changeMutator();break;
		        	case changeCrossover: changeCrossover();break;
		        	case setCrossoverParam: setCrossoverParam();break;
		        	case setMutatorParam: setMutatorParam();break;
		        	case setPopulationSize: setPopulationSize();break;
		        	case setOutputOnFile: selectedOutput=outputOptions.file; break;
		        	case setOutputOnTeminal: selectedOutput=outputOptions.terminal; break;
		        	case experimentCampaign: runCampaign();break;
		        	case printConfiguration: printConfiguration();break;
		        	case close: go=false;
	        	}
	        }

	    }
	    
	    private void printConfiguration() throws IOException {
	    	File f=new File ("test.configuration");
	    	OutputStream out = new FileOutputStream( f );
	    	configuration.store(out, "Experiment Configuration File");
	    }
	    private void changeAlgorithm() {
	        TextIO textIO = new TextIO(terminal);
	        
	        algorithms a = textIO.newEnumInputReader(algorithms.class)
	                .read("Which algorithm do you want to run?");
	        configuration.setProperty("DeployerType", ""+a);
	    }
	    
	    private void changeSelector() {
	        TextIO textIO = new TextIO(terminal);
	        
	        Selectors a = textIO.newEnumInputReader(Selectors.class)
	                .read("Which algorithm do you want to run?");
	        configuration.setProperty("Selector", ""+a);
	    }

	    private void changeMutator() {
	        TextIO textIO = new TextIO(terminal);
	        
	        Mutators a = textIO.newEnumInputReader(Mutators.class)
	                .read("Which algorithm do you want to run?");
	        configuration.setProperty("Mutator", ""+a);
	    }

	    private void changeCrossover() {
	        TextIO textIO = new TextIO(terminal);
	        
	        Crossovers a = textIO.newEnumInputReader(Crossovers.class)
	                .read("Which algorithm do you want to run?");
	        configuration.setProperty("Crossover", ""+a);
	    }

	    private void selectMACM() throws noAvailableFilesException {
	        TextIO textIO = new TextIO(terminal);
	        File folder = new File(MACMpath);
	        File[] listOfFiles = folder.listFiles();
	        List<String> filenames=new ArrayList<String>();
	        for (int i = 0; i < listOfFiles.length; i++) {
	          if (listOfFiles[i].isFile()) {
	            filenames.add(listOfFiles[i].getName());
	          }
	        }
	        if (filenames==null) throw new noAvailableFilesException();
	        String macmfile = textIO.newStringInputReader()	  
	        		.withNumberedPossibleValues(filenames)
	        		.read("Select a file");
	        configuration.setProperty("MACMfile", MACMpath+macmfile);
	    }
	    
	    private void selecOffering() throws noAvailableFilesException {
	        TextIO textIO = new TextIO(terminal);
	        
	        File folder = new File(OfferingPath);
	        File[] listOfFiles = folder.listFiles();
	        List<String> filenames=new ArrayList<String>();
	        if (filenames==null) throw new noAvailableFilesException();
	        for (int i = 0; i < listOfFiles.length; i++) {
	          if (listOfFiles[i].isFile()) {
	            filenames.add(listOfFiles[i].getName());
	          }
	        }
	        
	        String file = textIO.newStringInputReader()	  
	        		.withNumberedPossibleValues(filenames)
	        		.read("Select a file");
	        configuration.setProperty("Offeringfile", OfferingPath+file);
	    }
	    
	    
	    private void setCrossoverParam() {
	        TextIO textIO = new TextIO(terminal);
	        
	        float a = textIO.newFloatInputReader()
	                .read("Which Crossover Param do you want ([0,1]?");
	        configuration.setProperty("CrossoverParam", ""+a);
	    }
	    
	    private void setMutatorParam() {
	        TextIO textIO = new TextIO(terminal);
	        
	        float a = textIO.newFloatInputReader()
	                .read("Which Mutator Param do you want ([0,1]?");
	        configuration.setProperty("MutatorParam", ""+a);
	    }
	    
	    private void setPopulationSize() {
	        TextIO textIO = new TextIO(terminal);
	        
	        float a = textIO.newFloatInputReader()
	                .read("Which Mutator Param do you want ([0,1]?");
	        configuration.setProperty("MutatorParam", ""+a);
	    }
	    
	    
	    private void runCampaign() throws MissingOfferingException, MissingComponentsException, IOException {
	        TextIO textIO = new TextIO(terminal);
	        Campaign c=new Campaign();
	        c.setExpConf(configuration);
	        
	        parameters a = textIO.newEnumInputReader(parameters.class)
	                .read("Which Param do you want vary during the campaign?");
//	        c.fullFactorOnEnum(""+a, Class.forName("eu.unicampania.rak.console.data."+a));
	        switch (a) {
        	case algorithms: c.fullFactorOnEnum("DeployerType", algorithms.class); break;
        	case Selectors: c.fullFactorOnEnum("Selector", Selectors.class); break;
        	case Mutators: c.fullFactorOnEnum("Mutator", Mutators.class); break;
        	case Crossovers: c.fullFactorOnEnum("Crossover", Crossovers.class); break;
	        }

	    }
	    private void show() throws FileNotFoundException {
    		TextIO t=new TextIO(terminal);
 	    	if (selectedOutput==outputOptions.terminal) {
	    		terminal.println(stringOut.toString());
	    	} else if (selectedOutput==outputOptions.file) {
	    		Date date = new Date() ;
	    		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss") ;
	    		String resultfile = "results/RESULT_"+dateFormat.format(date) + ".result";
	    		File file = new File(resultfile) ;
	    		PrintWriter p=new PrintWriter(file);
	    		p.print(stringOut.toString());
	    		p.close();
	    		terminal.println("Output in file:"+resultfile);
	    	}
 	   		terminal.dispose();
	    }
	    
	    protected Properties readConfiguration() throws IOException{
	    	File f=new File("test.configuration");
	    	FileInputStream in = new FileInputStream( f );
	    	configuration.load(in);
	    	return configuration;
	    }
	public static void main(String args[]) throws MissingOfferingException, MissingComponentsException, IOException, noAvailableFilesException {
		OptimizationConsole c=new OptimizationConsole();		
		if (args.length>0) {
			if (args[0].equals("run")) {
				Properties p=c.readConfiguration();
				c.run(p);
				c.setOutputonFile();
				c.show();
			}
		} else {
			c.actionSelector();
		}
	}
}
