package eu.unicampania.rak.console.data;

public enum Mutators {
	Mutator, 
	GaussianMutator,
	SwapMutator
}
