package eu.unicampania.rak.console.data;

public enum Crossovers {
	IntermediateCrossover, 
	LineCrossover, 
	MultiPointCrossover, 
	PartiallyMatchedCrossover, 
	UniformCrossover		
}
