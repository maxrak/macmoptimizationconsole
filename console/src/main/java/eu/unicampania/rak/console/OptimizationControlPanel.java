package eu.unicampania.rak.console;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Properties;
import java.util.Scanner;

import eu.musaproject.multicloud.engines.deployments.MissingComponentsException;
import eu.musaproject.multicloud.engines.deployments.MissingOfferingException;
import eu.unicampania.rak.console.actions.Experiment;

public class OptimizationControlPanel {
	//Optimizer Types
	static String[] deployers= {"GeneratorOptimizer2","GeneticOptimizer2"};
	
	//Genetic Alternatives
	static String[] selectors={"BoltzmannSelector", "EliteSelector", "ExponentialRankSelector", "LinearRankSelector", "MonteCarloSelector", "ProbabilitySelector", "RouletteWheelSelector", "StochasticUniversalSelector", "TournamentSelector", "TruncationSelector"};
	static String[] mutators={"Mutator", "GaussianMutator", "SwapMutator"};
	static String[] crossovers={"IntermediateCrossover", "LineCrossover", "MultiPointCrossover", "PartiallyMatchedCrossover", "UniformCrossover"};		
	static String[] params={"0.1","0.2","0.3","0.4","0.5","0.6","0.7","0.8","0.9"};		

	//problems
	static String[] macms={"1","311","535"};
	static String[] offerings={"5","10","15"};

	Experiment exp;
	PrintWriter output;
	Properties configuration;
	
	public OptimizationControlPanel() throws MissingOfferingException, MissingComponentsException {
		exp=new Experiment();
		PrintWriter output = new PrintWriter(System.out);
		configuration=defaultConf();
	}
	
	protected void run(Properties p) throws IOException, MissingOfferingException, MissingComponentsException {
		exp.configure(p);
		String optData=exp.run();
		String problem=exp.problemDescriptor();
		String solution=exp.getSolution();
		output.println("----------------");
		output.println(problem);
		output.println(solution);
		output.println(optData);
	}
	
	protected void run() throws IOException, MissingOfferingException, MissingComponentsException {
		run(configuration);
	}
		
	protected PrintWriter setOutput(String out) throws FileNotFoundException {
		if (out.equalsIgnoreCase("stdout")) 
			output = new PrintWriter(System.out);
		else {
			File file = new File(out);
			output = new PrintWriter(file);
		}			
		return output;
	}
	
	public static void clearScreen() {  
	    System.out.print("\033[H\033[2J");  
	    System.out.flush();  
	}
	
	protected void showOptions(){
		int i;
		System.out.println("**** MACM Optimizer ****");
		System.out.println("*** Options:");
		System.out.println("** Algorithms: " );
		for (i=0;i<deployers.length;i++) {
			System.out.print(" ["+i+"] "+deployers[i]);
		}
		System.out.println("");

		System.out.println("*MACMs: ");
		for (i=0;i<macms.length;i++) {
			System.out.print(" ["+i+"] "+macms[i]);
		}
		System.out.println("");

		System.out.println("*Offerings: " );
		for (i=0;i<offerings.length;i++) {
			System.out.print( "["+i+"] "+offerings[i]);
		}
		System.out.println("");
		System.out.println("** Genetic Algorithms Parameters " );
		System.out.println("*Selector " );
		for (i=0;i<selectors.length;i++) {
			System.out.print(" ["+i+"] "+selectors[i]);
		}
		System.out.println("");
		System.out.println("*Crossover " );
		for (i=0;i<crossovers.length;i++) {
			System.out.print(" ["+i+"] "+crossovers[i]);
		}
		System.out.println("");
		System.out.println("*Crossover Param " );
		for (i=0;i<params.length;i++) {
			System.out.print(" "+params[i]);
		}
		System.out.println("");
		System.out.println("*Mutator " );
		for (i=0;i<mutators.length;i++) {
			System.out.print(" ["+i+"] "+mutators[i]);
		}
		System.out.println("");
		System.out.println("*Mutator Param " );
		for (i=0;i<params.length;i++) {
			System.out.print(" "+params[i]);
		}
		System.out.println("");
		System.out.println("**** MACM Optimizer ****");
	}
	
	protected void showConfiguration(Properties p){
		int i;
		clearScreen();
		System.out.println("**** MACM Optimizer ****");

		System.out.println("*Algorithms: "+p.getProperty("DeployerType"));
		System.out.println("*MACMs: "+p.getProperty("MACMfile"));
		System.out.println("*Offerings: "+p.getProperty("Offeringfile") );
		System.out.println("** Genetic Algorithms Parameters ");
		System.out.println("*Population Size " +p.getProperty("PopulationSize"));
		System.out.println("*Selector " +p.getProperty("Selector"));
		System.out.println("*Crossover "+p.getProperty("Crossover") );
		System.out.println("*Crossover Param " +p.getProperty("CrossoverParam"));
		System.out.println("*Mutator "+p.getProperty("Mutator") );
		System.out.println("*Mutator Param "+p.getProperty("MutatorParam") );
		System.out.println("**** MACM Optimizer ****");
	}
	
	protected void showConfiguration() {
		showConfiguration(configuration);
	}
	
	public Properties defaultConf() {
		Properties p=new Properties();
		p.put("MACMfile","optimization/macms/311.macm");
		p.put("Offeringfile", "optimization/offerings/10.offering");
		p.setProperty("DeployerType",deployers[0]);		
		p.setProperty("PopulationSize", "500");
		p.setProperty("Selector", "TruncationSelector");
		p.setProperty("Crossover", "SinglePointCrossover");
		p.setProperty("Mutator", "Mutator");
		p.setProperty("CrossoverParam", "0.1");
		p.setProperty("MutatorParam", "0.3");
		return p;
	}
	
	public void setConfiguration(Properties p) {
		configuration=p;
	}
	
	 protected int request() {
		int i=0;
		String action;
		Scanner scan=new Scanner(System.in);
		System.out.println("*********************");
		System.out.println("** Actions:");
		System.out.println("* 1. Run ");
		System.out.println("* 2. Change Deployer ");
		System.out.println("* 3. Change Genetic Conf ");
		System.out.println("* Select your choice: ");
		i=scan.nextInt();		
		return i;
	}
	 
	public static void main(String agrs[]) throws MissingOfferingException, MissingComponentsException, IOException {
		int i;
		OptimizationControlPanel panel=new OptimizationControlPanel();	
		panel.defaultConf();
		clearScreen();
		panel.showOptions();
		panel.showConfiguration();
		i=panel.request();
		if(i==1) {
			panel.run();
		}
	}
}
