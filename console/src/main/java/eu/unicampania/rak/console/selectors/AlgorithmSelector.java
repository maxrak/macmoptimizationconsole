package eu.unicampania.rak.console.selectors;

import java.util.function.BiConsumer;

import org.beryx.textio.TextIO;
import org.beryx.textio.TextIoFactory;
import org.beryx.textio.TextTerminal;
import org.beryx.textio.web.RunnerData;

import eu.unicampania.rak.console.data.algorithms;
import eu.unicampania.rak.console.textIOdemo.app.AppUtil;


public class AlgorithmSelector implements BiConsumer<TextIO, RunnerData> {
    public static void main(String[] args) {
        TextIO textIO = TextIoFactory.getTextIO();
        new AlgorithmSelector().accept(textIO, null);
    }

    public void accept(TextIO textIO, RunnerData runnerData) {
        TextTerminal<?> terminal = textIO.getTextTerminal();
        String initData = (runnerData == null) ? null : runnerData.getInitData();
        AppUtil.printGsonMessage(terminal, initData);

        algorithms alg = textIO.newEnumInputReader(algorithms.class)
                .read("Which algorithm do you want to use?");

        terminal.printf("Algorithm selected %s", alg);

        textIO.newStringInputReader().withMinLength(0).read("\nPress enter to terminate...");
        textIO.dispose("bye.");
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + ": select the Algorithm";
    }
}