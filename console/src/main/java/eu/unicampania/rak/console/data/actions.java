package eu.unicampania.rak.console.data;

public enum actions {
	showConfiguration,
	run,
	selectMACM,
	selectOffering,
	changeAlgorithm,
	changeSelector,
	changeMutator,
	changeCrossover,
	setCrossoverParam,
	setMutatorParam,
	setPopulationSize,
	setOutputOnFile,
	setOutputOnTeminal,
	experimentCampaign,
	printConfiguration,
	close
}
