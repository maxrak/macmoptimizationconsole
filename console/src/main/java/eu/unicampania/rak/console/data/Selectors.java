package eu.unicampania.rak.console.data;

public enum Selectors {
	BoltzmannSelector,
	EliteSelector,
	ExponentialRankSelector,
	LinearRankSelector,
	MonteCarloSelector,
	ProbabilitySelector,
	RouletteWheelSelector,
	StochasticUniversalSelector,
	TournamentSelector,
	TruncationSelector
}
