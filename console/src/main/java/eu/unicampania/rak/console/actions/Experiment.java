package eu.unicampania.rak.console.actions;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Properties;
import java.util.Scanner;

import eu.musaproject.multicloud.engines.deployments.Deployer;
import eu.musaproject.multicloud.engines.deployments.Deployment;
import eu.musaproject.multicloud.engines.deployments.DeploymentInterface;
import eu.musaproject.multicloud.engines.deployments.DeploymentMapBased;
import eu.musaproject.multicloud.engines.deployments.MissingComponentsException;
import eu.musaproject.multicloud.engines.deployments.MissingOfferingException;
import eu.musaproject.multicloud.engines.deployments.WorkInProgressException;
import eu.musaproject.multicloud.engines.deployments.optimization.GeneticOptimizer2;
import eu.musaproject.multicloud.models.macm.MACM;
import eu.musaproject.multicloud.utilities.NeoDriver;

public class Experiment {
	boolean ready=false;
	MACM abstractMACM;
	MACM deployedMACM=null;
	String OfferingFile="optimization/default/offering.properties";
	String ComponentsFile="optimization/default/components.properties";
	String MACMfile="optimization/default/macm.cyber";
	String deployerName="GeneratorOptimizer2";
	DeploymentInterface myDeployment;
	Deployer deployer;

	public Experiment() throws MissingOfferingException, MissingComponentsException {
		setDeployer(deployerName);
		//		abstractMACM=new MACM();
		//		deployer.setMACM(abstractMACM);
		//		myDeployment=deployer.getDeployment();
		//		myDeployment.readOfferings(OfferingFile);	
		//myDeployment.readComponents(ComponentsFile);
	}

	public Experiment(String deployerType) {
		deployerName=deployerType;
		setDeployer(deployerName);
		//		abstractMACM=new MACM();
		//		deployer.setMACM(abstractMACM);
		//		myDeployment=deployer.getDeployment();
		//		myDeployment.readOfferings(OfferingFile);	
		//myDeployment.readComponents(ComponentsFile);
	}	

	public Experiment(DeploymentInterface dep) throws MissingOfferingException, MissingComponentsException {
		myDeployment=dep;		
		abstractMACM=new MACM();
		myDeployment.setApp(abstractMACM);
		deployer=setDeployer(deployerName);
	}

	public Experiment(DeploymentInterface dep,MACM macm) throws MissingOfferingException, MissingComponentsException {
		myDeployment=dep;		
		abstractMACM=macm;
		myDeployment.setApp(abstractMACM);
		deployer=setDeployer(deployerName);
	}

	public Experiment(DeploymentInterface dep,MACM macm,String deployerType) throws MissingOfferingException, MissingComponentsException {
		myDeployment=dep;		
		abstractMACM=macm;
		myDeployment.setApp(abstractMACM);
		deployerName=deployerType;
		deployer=setDeployer(deployerName);
	}

	protected MACM setMACM() throws IOException, MissingOfferingException, MissingComponentsException {
		NeoDriver.cleanDB();
		NeoDriver.executeFromFile(MACMfile);
		abstractMACM = new MACM();
		abstractMACM.readNeo();
		deployer.setMACM(abstractMACM);
		return abstractMACM;		
	}

	protected MACM setMACM(String file) throws IOException, MissingOfferingException, MissingComponentsException {
		MACMfile=file;
		return setMACM();
	}

	protected void setOfferings() {
		deployer.setOfferings(OfferingFile);
	}

	protected void setOfferings(String file) {
		OfferingFile=file;
		deployer.setOfferings(OfferingFile);
	}

	protected void setComponents() throws WorkInProgressException  {
		throw new WorkInProgressException();
	}
	protected Deployer setDeployer(String name){
		Class c;
		try {
			c = Class.forName("eu.musaproject.multicloud.engines.deployments.optimization."+name);
			System.out.println(c);
			Constructor cons = c.getConstructor();
			deployer = (Deployer) cons.newInstance();
		} catch (ClassNotFoundException e) {
			System.out.println("Deployer "+name+" is not yet supported");
			e.printStackTrace();
			System.exit(0);
		} catch (NoSuchMethodException e) {
			System.out.println("Deployer "+name+" No Constructor without parameters");
			e.printStackTrace();
		} catch (SecurityException e) {
			System.out.println("Deployer "+name+" Reflection Security issues");
			e.printStackTrace();
		} catch (InstantiationException e) {
			System.out.println("Deployer "+name+" Reflection Error creating the object");
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			System.out.println("Deployer "+name+" Reflection Illegal access");
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			System.out.println("Deployer "+name+" Reflection Illegal argument");
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			System.out.println("Deployer "+name+" Reflection Error Inovking");
			e.printStackTrace();
		}
		return deployer;
	}

	public String run() throws MissingOfferingException, MissingComponentsException {
		//deployer.configure();
		String result="unready";
		if(ready) {
			deployer.setMACM(abstractMACM);
			deployer.setOfferings(OfferingFile);
			deployer.deploy();
			myDeployment=deployer.refreshDeployment();
			deployedMACM= deployer.getDeployable();
			result=deployer.getOptimizationData();
		}
		return result;
	}

	public void configure(Properties prop) throws IOException, MissingOfferingException, MissingComponentsException {
		setDeployer(prop.getProperty("DeployerType"));
		setMACM(prop.getProperty("MACMfile"));
		setOfferings(prop.getProperty("Offeringfile"));
		ready=true;
	}

	public void printResult() {
		System.out.println("----------------------------");
		System.out.println("ProblemDimension nComponents:"+myDeployment.getComponents().length+" nOfferings:"+myDeployment.getOffering().length);
		System.out.print("Final Result:");
		myDeployment.print();
		deployer.printOptimizationData();
	}

	public String problemDescriptor() {
		return deployer.problemDescriptor();
	}

	public String getSolution() {
		return deployer.getOptimizedSolution();
	}

	public static void main(String agrs[]) throws MissingOfferingException, MissingComponentsException, IOException {
		Experiment exp=new Experiment();
		Properties p=new Properties();
		
		//Optimizer Types
		String[] deployers= {"GeneratorOptimizer2","GeneticOptimizer2"};
		
//		//Genetic Alternatives
//		String[] selectors={"BoltzmannSelector", "EliteSelector", "ExponentialRankSelector", "LinearRankSelector", "MonteCarloSelector", "ProbabilitySelector", "RouletteWheelSelector", "StochasticUniversalSelector", "TournamentSelector", "TruncationSelector"};
//		String[] mutators={"Mutator", "GaussianMutator", "SwapMutator"};
//		String[] crossovers={"IntermediateCrossover", "LineCrossover", "MultiPointCrossover", "PartiallyMatchedCrossover", "UniformCrossover"};		
//		String[] params={"0.1","0.2","0.3","0.4","0.5","0.6","0.7","0.8","0.9"};		
//		
//		//problems
//		String[] macms={"1","311","535"};
//		String[] offerings={"5","10","15"};
		p.put("MACMfile","optimization/macms/311.macm");
		p.put("Offeringfile", "optimization/offerings/10.offering");
		for (int i=0;i<deployers.length;i++) {
			p.setProperty("DeployerType",deployers[i]);
			exp.configure(p);
			String optData=exp.run();
			String problem=exp.problemDescriptor();
			String solution=exp.getSolution();
			System.out.println("----------------");
			System.out.println(problem);
			System.out.println(solution);
			System.out.println(optData);
		}
	}



	public static void oldmain(String args[]) throws MissingOfferingException, MissingComponentsException {
		int i,N=1;//50;
		double[] costs=new double[N];
		int[] evaluations=new int[N];
		double cmean=0;
		int evmean=0;
		String c="",ev="";
		try {
			for(i=0;i<N;i++) {
				NeoDriver.cleanDB();
				NeoDriver.executeFromFile("src/main/resources/Models/TUTabstract");
				MACM macm= new MACM();
				macm.readNeo();
				//				Deployer deployer=new GeneratorOptimizer();
				Deployer deployer=new GeneticOptimizer2();
				deployer.setOfferings("src/main/resources/optimization/test1.properties");
				deployer.setMACM(macm);
				deployer.setOfferings("src/main/resources/optimization/test1.properties");
				Scanner scan= new Scanner (System.in);
				System.out.println("The abstract application is in Graph DB,  press enter to deploy2");
				scan.nextLine();
				deployer.deploy();
				deployer.refreshDeployment();
				MACM m2=deployer.getDeployable();
				NeoDriver.cleanDB();
				m2.writeNeo();
				deployer.printDeployment();
				costs[i]=deployer.getFinalcost();
				c+=" "+String.format( "%1$,.2f",costs[i]);
				cmean+=costs[i];
				evaluations[i]=deployer.getEvaluated();
				ev+=" "+evaluations[i];
				evmean+=evaluations[i];
			}
			System.out.println("Mean Cost:"+String.format( "%1$,.2f",cmean/N)+" Costs:"+c);
			System.out.println("Mean Evaluations:"+(evmean/N)+" Evaluations:"+ev);
			System.exit(0);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
