package eu.unicampania.rak.console.textIOdemo;

/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import org.beryx.textio.*;
import org.beryx.textio.console.ConsoleTextTerminalProvider;
import org.beryx.textio.jline.JLineTextTerminalProvider;
import org.beryx.textio.swing.SwingTextTerminalProvider;
import org.beryx.textio.system.SystemTextTerminal;
import org.beryx.textio.system.SystemTextTerminalProvider;

import org.beryx.textio.web.WebTextTerminal;

import eu.unicampania.rak.console.selectors.AlgorithmSelector;
import eu.unicampania.rak.console.selectors.CrossoverSelector;
import eu.unicampania.rak.console.textIOdemo.app.UserDataCollector;

import org.beryx.textio.web.*;

import java.util.Arrays;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Supplier;

/**
 * Demo application showing various TextTerminals.
 */
public class OptimizationConsoleOlder {

    public static void main(String[] args) {
    	OptimizationConsoleOlder c=new OptimizationConsoleOlder();
    	c.ActionSelector();
    }

    private static void ActionSelector() {
        SystemTextTerminal sysTerminal = new SystemTextTerminal();
        TextIO sysTextIO = new TextIO(sysTerminal);
        BiConsumer<TextIO, RunnerData> app = chooseSelector(sysTextIO);
        app.accept(sysTextIO, null);
    }
    
    private static BiConsumer<TextIO, RunnerData> chooseSelector(TextIO textIO) {
        List<BiConsumer<TextIO, RunnerData>> apps = Arrays.asList(
                new AlgorithmSelector(),
                new CrossoverSelector()
        );
        BiConsumer<TextIO, RunnerData> app = textIO.<BiConsumer<TextIO, RunnerData>>newGenericInputReader(null)
            .withNumberedPossibleValues(apps)
            .read("Choose the application to be run");
        String propsFileName = app.getClass().getSimpleName() + ".properties";
        System.setProperty(AbstractTextTerminal.SYSPROP_PROPERTIES_FILE_LOCATION, propsFileName);

        return app;
    }

}