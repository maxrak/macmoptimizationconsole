# MACM Optimization Console #

This software package contains all the tools needed to execute optimization algorithms for MACM application models

## Dependencies ##

### Environment ###
* Neo4j

### Development ###
* Maven

## Binary Distribution ##
In the Download Section you can find a opt.zip file.
The only requirement is to have neo4j already installed.

* Unzip the file opt.zip
* Execute the run.sh script

The binary distribution contains 3 example macm files and 4 offering files.
